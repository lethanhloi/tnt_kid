import sentencepiece as spm
from preprocessing import file_to_df
import os
from nltk import sent_tokenize
from bpe.bpe import *
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, default='news_vie.json')
    parser.add_argument('--output', type=str, default='bpe_news_vie')
    args = parser.parse_args()
    train_bpe_model(args.input, args.output)
