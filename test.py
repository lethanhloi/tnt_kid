import multiprocessing
from itertools import product
import torch


def merge_names(a, b):
    return '{} & {}'.format(a, b)

if __name__ == '__main__':
    # names = ['Brown', 'Wilson', 'Bartlett', 'Rivera', 'Molloy', 'Opie']
    # with multiprocessing.Pool(processes=3) as pool:
    #     results = pool.starmap(merge_names, product(names, repeat=2))
    # print(results)

    x = torch.randn(2,3,4)
    print(x, x.size())
    #
    # x = torch.cat((x,x,x), dim= -1)
    #
    # print(x, x.shape)
    print(x.squeeze(0), x.size())
    print(x.unsqueeze(0), x.unsqueeze(0).size())
    print(x.flatten(2), x.flatten(2).size())
