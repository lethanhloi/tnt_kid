import sentencepiece as spm
from preprocessing import file_to_df
import os
from nltk import sent_tokenize
import argparse

# root_path = "../data/"
root_path = "data/"
max_line = 100000


def train_bpe_model(input, output):
    df = file_to_df(os.path.join(root_path + input), bpe=True, classification=False)
    with open(root_path + output + '.txt', 'w+', encoding='utf8') as f:
        for idx, line in df.iterrows():
            if idx > max_line:
                break
            text = line['text']
            sents = sent_tokenize(text)
            for sent in sents:
                f.write(sent.lower().strip() + '\n')

    assert not os.path.exists(output + '.model')

    spm.SentencePieceTrainer.Train('--input=' + root_path + output + '.txt --model_prefix=' +
                                   output + ' --vocab_size=32000 --character_coverage=1.0')

    sp = spm.SentencePieceProcessor()
    sp.Load(output + ".model")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, default='news_vie.json')
    parser.add_argument('--output', type=str, default='bpe_news_vie')
    args = parser.parse_args()
    train_bpe_model(args.input, args.output)
