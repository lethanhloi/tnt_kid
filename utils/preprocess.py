from vncorenlp import VnCoreNLP
from time import time
import logging
import os

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def tokenizer_vncorenlp(text, annotator):
    try:
        text_tokenizer = ""
        word_segmented_text = annotator.tokenize(text)
        for sentence in word_segmented_text:
            words_segmented = " ".join(sentence)
            text_tokenizer = text_tokenizer + " " + words_segmented
        return text_tokenizer
    except:
        return text


def load_annotator():
    logger.info("Loading annotator of vncorenlp for segmentation of text")
    start_time = time()
    annotator = VnCoreNLP(address="http://127.0.0.1", port=9000)
    # absolute_path = os.path.abspath("../vncorenlp/VnCoreNLP-1.1.1.jar")
    # annotator = VnCoreNLP(absolute_path, annotators="wseg,pos,ner,parse", max_heap_size='-Xmx2g')
    logger.info('Time elapsed {} seconds'.format((time() - start_time)))
    return annotator


def clean_text(text):
    return text


def preprocess_text(annotator, text):
    # annotator = load_annotator()
    text = clean_text(text)
    text = tokenizer_vncorenlp(text, annotator)
    # print(text)
    return text


if __name__ == '__main__':
    text = "Ông Nguyễn Khắc Chúc  đang làm việc tại Đại học Quốc gia Hà Nội. Bà Lan, vợ ông Chúc, cũng làm việc tại đây."
    annotator = load_annotator()
    preprocess_text(annotator, text)
    # absolute_path = os.path.abspath("../vncorenlp/VnCoreNLP-1.1.1.jar")
    # print(absolute_path)
