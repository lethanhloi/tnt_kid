import os
import random, json
from tqdm import tqdm
import logging
from utils.preprocess import *
import multiprocessing
from itertools import product

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def read_data(path):
    data = []
    if os.path.isdir(path):
        for file_name in os.listdir(path):
            with open(os.path.join(path, file_name), 'r') as f:
                lines = f.readlines()
                for line in lines:
                    data.append(line)

    else:
        with open(path, 'r') as f:
            lines = f.readlines()
            for line in lines:
                data.append(line)
    return data


def preprocess(data):
    keywords_processed = []
    keywords = data["keyphrases"]
    for key in keywords:
        keywords_processed.append(
            preprocess_text(annotator, key)
        )
    text_processed = preprocess_text(annotator, text=data["text"])

    result = {
        "keyphrases": keywords_processed,
        "text": text_processed,
        "postags": data["postags"]
    }

    return result


def take_data_keyphrase(path):
    data = []

    if os.path.isdir(path):
        data_keyphrases = read_data(path + "/keywords")
        data_postag = read_data(path + "/postags")
        data_text = read_data(path + "/words")
    else:
        data_keyphrases = read_data("../data/news_vie/keywords/" + path)
        data_postag = read_data("../data/news_vie/postags/" + path)
        data_text = read_data("../data/news_vie/words/" + path)

    for i in tqdm(range(len(data_keyphrases))):
        keywords = data_keyphrases[i].split(",")
        # keywords_processed = []
        # for key in keywords:
        #     keywords_processed.append(
        #         preprocess_data(annotator, key)
        #     )
        # text_processed = preprocess_data(annotator, text=data_text[i])

        # todo : update
        keywords_processed = keywords
        text_processed = data_text[i]
        data.append({
            "keyphrases": keywords_processed,
            "postags": data_postag[i],
            "text": text_processed
        })

    # #todo: add multi processing
    # with multiprocessing.Pool(processes=3) as pool:
    #     results = pool.starmap(preprocess, product(names, repeat=2))
    # print(results)

    return data


def convert_to_tnt_format_file(text):
    try:
        data_convert = {
            "id": "",
            "title": "",
            "abstract": text["text"],
            "keywords": text["keyphrases"],
        }
        return data_convert
    except:
        return None


def build_data_train_valid_keyphrase(data, train_ratio=0.9, test_ratio=0.05, valid_ratio=0.05):
    random.shuffle(data)
    train_data = data[: int(train_ratio * (len(data)))]
    test_valid_data = data[len(train_data):]
    test_ratio_ = test_ratio / (test_ratio + valid_ratio)
    test_data = test_valid_data[:int(test_ratio_ * (len(test_valid_data)))]
    valid_data = test_valid_data[len(test_data):]
    logger.info(
        "\nNumber document of :\n - train data : {train} \n - test_data : {test} \n - valid_data : {valid} ".format(
            train=len(train_data), test=len(test_data), valid=len(valid_data)))

    logger.info("Process training dataset")
    with open('../data/news_vie/news_vie_train.json', 'w+') as writer:
        for text in tqdm(train_data):
            data_convert = convert_to_tnt_format_file(text)
            writer.write(json.dumps(data_convert, ensure_ascii=False) + "\n")

    logger.info("Process test dataset")
    with open('../data/news_vie/news_vie_test.json', 'w+') as writer:
        for text in tqdm(test_data):
            data_convert = convert_to_tnt_format_file(text)
            writer.write(json.dumps(data_convert, ensure_ascii=False) + "\n")

    logger.info("Process valid dataset")
    with open('../data/news_vie/news_vie_valid.json', 'w+') as writer:
        for text in tqdm(valid_data):
            data_convert = convert_to_tnt_format_file(text)
            writer.write(json.dumps(data_convert, ensure_ascii=False) + "\n")


if __name__ == '__main__':
    # annotator = load_annotator()
    data = take_data_keyphrase("../data/news_vie")
    build_data_train_valid_keyphrase(data)
